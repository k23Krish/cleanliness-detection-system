"use strict";
const Complain = require("../models/complain.model");

exports.create = function (req, res) {
	const new_complain = new Complain(req.body);
	//handles null error
	if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
		res.status(400).send({ error: true, message: "Please provide all required field" });
	} else {
		Complain.create(new_complain, function (err, complain) {
			if (err) res.send(err);
			res.json({ error: false, message: "Complain added successfully!", data: complain });
		});
	}
};

exports.findAll = function (req, res) {
	Complain.findAll(function (err, complain) {
		if (err) res.send(err);
		res.send(complain);
	});
};

exports.findUnclean = function (req, res) {
	Complain.findUnclean(function (err, complain) {
		if (err) res.send(err);
		res.send(complain);
	});
};

exports.findUncleanAssigned = function (req, res) {
	Complain.findUncleanAssigned(function (err, complain) {
		if (err) res.send(err);
		res.send(complain);
	});
};

exports.findByGroup = function (req, res) {
	Complain.findByGroup(function (err, complain) {
		console.log("controller");
		if (err) res.send(err);

		res.json(complain);
	});
};

exports.findByLocation = function (req, res) {
	Complain.findByLocation(req.params.location, function (err, complain) {
		if (err) res.send(err);
		res.json(complain);
	});
};

exports.cleaned = function (req, res) {
	Complain.cleaned(req.params.location, function (err, complain) {
		if (err) res.send(err);
		Complain.cleaned1(req.params.location, function (err, complain) {
			if (err) res.send(err);
			res.json({ error: false, message: "Complain successfully edited" });
		});
	});
};
