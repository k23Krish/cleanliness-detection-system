"use strict";
const Worker = require("../models/workers.model");

exports.create = function (req, res) {
	const new_worker = new Worker(req.body);
	//handles null error
	if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
		res.status(400).send({ error: true, message: "Please provide all required field" });
	} else {
		Worker.create(new_worker, function (err, worker) {
			if (err) res.send(err);
			res.json({ error: false, message: "Complain added successfully!", data: worker });
		});
	}
};

exports.login = (req, res) => {
	console.log(res.body);
	const cred = {
		email: req.body.email,
		password: req.body.password
	};

	if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
		res.status(400).send({ error: true, message: "Please provide all required field" });
	} else {
		Worker.login(cred, function (err, worker) {
			if (err) {
				res.send(err);
			} else if (worker.length === 0) {
				res.json({ error: true, message: "NO User Found", data: worker });
			} else {
				res.json({ error: false, message: "User Found", data: worker });
			}
		});
	}
};

exports.assign = (req, res) => {
	if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
		res.status(400).send({ error: true, message: "Please provide all required field" });
	} else {
		const data = {
			location: req.params.location,
			lat: req.params.lat,
			long: req.params.long,
			workers: req.body.workers
		};

		Worker.assign(data, function (err, worker) {
			if (err) res.send(err);
		});
		Worker.assign1(data, function (err, worker) {
			if (err) res.send(err);
			res.json({ error: false, message: "Worker Assigned Successfully" });
		});
	}
};

exports.get_avail = (req, res) => {
	Worker.get_avail(function (err, worker) {
		console.log("controller");
		if (err) res.send(err);
		console.log("res", worker);
		res.send(worker);
	});
};

exports.getId = (req, res) => {
	Worker.getId(req.params.id, function (err, worker) {
		console.log("controller");
		if (err) res.send(err);
		console.log("res", worker);
		res.send(worker);
	});
};

exports.record = (req, res) => {
	Worker.record(req.params.location, function (err, worker) {
		if (err) res.send(err);
		res.json(worker);
	});
};
