// modules imports
const express = require("express");
const bodyParser = require("body-parser");
const chalk = require("chalk");
const cors = require("cors");
const path = require("path");

// components imports
const complainRoutes = require("./routers/complain.routes");
const workerRoutes = require("./routers/workers.routes");
const port = process.env.PORT || 8000;

// app setup
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, "../../", "build")));

// custom routes
app.use("/api/complain", complainRoutes);
app.use("/api/workers", workerRoutes);

// routes default
app.get("/", function (req, res) {
	res.sendFile(path.join(__dirname, "build", "index.html"));
});

// server listener
app.listen(port, () => {
	console.log(chalk.black.bgGreen("Connection on PORT " + port + "!!!"));
});
