"use strict";
var dbConn = require("./../../config/db.config");

//Complain object create
var Complain = function (complain) {
	this.id = complain.id;
	this.email = complain.email;
	this.phone = complain.phone;
	this.location = complain.location;
	this.lat = complain.lat;
	this.long = complain.long;
	this.enviornment_type = complain.enviornment_type;
	this.assigned_to = complain.assigned_to;
	this.cleaned = complain.status ? complain.status : 0;
	this.assigned = complain.assigned;
	this.created_on = new Date();
	this.updated_on = new Date();
};

Complain.create = function (newComp, result) {
	let query = `SELECT * FROM complain WHERE location = '${newComp.location}'`;
	dbConn.query(query, function (err, res) {
		if (err) {
			result(err, null);
		} else {
			if (res.length > 0) {
				let query1 = `UPDATE complain SET total_complain = total_complain + 1, email = '${newComp.email}', phone= '${newComp.phone}',enviornment_type= '${newComp.enviornment_type}' WHERE location = '${newComp.location}'`;
				dbConn.query(query1, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else {
						console.log(res.insertId);
						result(null, res.insertId);
					}
				});
			} else {
				dbConn.query("INSERT INTO complain set ?", newComp, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else {
						console.log(res.insertId);
						result(null, res.insertId);
					}
				});
			}
		}
	});
};

Complain.findAll = function (result) {
	dbConn.query("Select * from complain where cleaned  = 1 ORDER BY total_complain DESC", function (err, res) {
		if (err) {
			result(null, err);
		} else {
			result(null, res);
		}
	});
};

Complain.findUnclean = function (result) {
	dbConn.query("Select * from complain where cleaned  != 1 AND assigned != 1 ORDER BY total_complain DESC", function (err, res) {
		if (err) {
			result(null, err);
		} else {
			result(null, res);
		}
	});
};

Complain.findUncleanAssigned = function (result) {
	dbConn.query("Select * from complain where cleaned  != 1 AND assigned = 1 ORDER BY total_complain DESC", function (err, res) {
		if (err) {
			result(null, err);
		} else {
			result(null, res);
		}
	});
};

Complain.findByGroup = function (result) {
	dbConn.query("Select * from complain GROUP BY location ORDER BY COUNT(total_complain) DESC ", function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

Complain.findByLocation = function (loc, result) {
	dbConn.query(`Select * from complain WHERE location = '${loc}' AND cleaned != '${1}'`, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

Complain.cleaned = function (loc, result) {
	let date = new Date();
	let today = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
	let query = `UPDATE complain SET cleaned = '${1}', assigned = '${0}', cleaned_on = '${today}' WHERE location = '${loc}'`;
	dbConn.query(query, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

Complain.cleaned1 = function (loc, result) {
	let query = `UPDATE workers SET assigned = '${0}' WHERE location_assigned = '${loc}'`;
	dbConn.query(query, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			result(null, res);
		}
	});
};

module.exports = Complain;
