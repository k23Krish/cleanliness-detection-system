"use strict";
var md5 = require("md5");
var dbConn = require("./../../config/db.config");

//Complain object create
var Worker = function (worker) {
	this.id = worker.id;
	this.name = worker.name;
	this.email = worker.email;
	this.phone = worker.phone;
	this.password = md5(worker.password);
	this.location_assigned = worker.location_assigned;
	this.lat = worker.lat;
	this.longi = worker.long;
	this.cleaned = worker.status ? worker.status : 0;
	this.created_at = new Date();
};

Worker.create = function (newWork, result) {
	dbConn.query("INSERT INTO workers set ?", newWork, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});
};

Worker.login = (cred, result) => {
	let query = `SELECT * from workers WHERE email = '${cred.email}' AND password = '${md5(cred.password)}'`;
	dbConn.query(query, function (err, res) {
		console.log(res);
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

Worker.assign = function (data, result) {
	let workers = data.workers.split(",");
	workers.forEach((id) => {
		let query = `UPDATE workers SET lat = '${data.lat}', longi='${data.long}', location_assigned = '${
			data.location
		}', assigned = '${1}' WHERE id = '${parseInt(id)}'`;
		dbConn.query(query, function (err, res) {
			if (err) {
				console.log("error: ", err);
				result(null, err);
			}
		});
	});
};

Worker.assign1 = function (data, result) {
	let date = new Date();
	let today = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`;
	let query1 = `UPDATE complain SET assigned = '${1}',assigned_to = '${data.workers}', assigned_on='${today}' WHERE location = '${data.location}'`;
	dbConn.query(query1, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log("Workers : ", res);
			result(null, res);
		}
	});
};

Worker.get_avail = function (result) {
	dbConn.query("Select * from workers where assigned = 0", function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log("Workers : ", res);
			result(null, res);
		}
	});
};

Worker.getId = function (id, result) {
	dbConn.query("Select * from workers where  id = ? ", id, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log("Workers : ", res);
			result(null, res);
		}
	});
};

Worker.record = function (loc, result) {
	dbConn.query("Select * from workers where location_assigned = ? ", loc, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

module.exports = Worker;
