const express = require("express");
const multer = require("multer");
const router = express.Router();

const complainController = require("../controllers/complain.controller");

// Create a new Complain
router.post("/create", complainController.create);

// Retrieve all Complains
router.get("/record", complainController.findAll);

// retrieve only unclean Areas and un Assigned Areas
router.get("/unClean", complainController.findUnclean);

// retrieve only unclean Areas and  Assigned Areas
router.get("/unClean_A", complainController.findUncleanAssigned);

// Retrieve Complains by Group of Lat and Long
// router.get("/groupBy", complainController.findByGroup);

// Retrieve a Complain with location
router.get("/:location", complainController.findByLocation);

// Change Complain status to Resolved
router.post("/area_cleaned/:location", complainController.cleaned);

// upload Image
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, "../../../ml_module/Upload_image");
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	}
});
var uploading = multer({ storage: storage }).single("upload");

router.post("/image", uploading, (req, res) => {
	res.send();
});

module.exports = router;
