const express = require("express");

const router = express.Router();

const workerController = require("../controllers/workers.controller");

// Create a new Complain
router.post("/create", workerController.create);

// login worker
router.post("/login", workerController.login);

// get all available workers
router.get("/get_avail", workerController.get_avail);

// assign locations
router.post("/assign/:location/:lat/:long", workerController.assign);

// get assigned workers for a location
router.get("/record/:location", workerController.record);

// get worker by ids
router.get("/:id", workerController.getId);

module.exports = router;
