"use strict";
const mysql = require("mysql");
const chalk = require("chalk");

//local mysql db connection
const dbConn = mysql.createConnection({
	host: "localhost",
	user: "root",
	password: "root",
	database: "c_m_s"
});
dbConn.connect(function (err) {
	if (err) throw err;
	console.log(chalk.black.bgCyan("Database Connected!"));
});

module.exports = dbConn;
