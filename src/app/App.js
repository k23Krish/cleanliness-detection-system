// imports
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useCookies } from "react-cookie";

// components
import Admin from "./main/admin";
import Worker from "./main/worker";
import Dashboard from "./main/admin/components/Dashboard";
import WorkerDashboard from "./main/worker/components/WorkerDashboard";
import User from "./main/user";

function App() {
	const [cookie, setCookie] = useCookies(["loginAdmin"]);
	const [cookie2, setCookie2] = useCookies(["loginWorker"]);
	const [cookie3, setCookie3] = useCookies(["worker"]);

	return (
		<Router>
			<Switch>
				<Route exact path="/">
					<User />
				</Route>
				<Route path="/login">
					<Admin cookie={cookie} setCookie={setCookie} />
				</Route>
				<Route path="/login_2">
					<Worker cookie={cookie2} setCookie={setCookie2} cookie2={cookie3} setCookie2={setCookie3} />
				</Route>
				<Route path="/admin">
					<Dashboard cookie={cookie} setCookie={setCookie} />
				</Route>
				<Route path="/worker_page">
					<WorkerDashboard cookie={cookie2} setCookie={setCookie2} cookie2={cookie3} setCookie2={setCookie3} />
				</Route>
			</Switch>
		</Router>
	);
}

export default App;
