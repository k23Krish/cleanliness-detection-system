// imports
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone";
import axios from "axios";
import { Redirect } from "react-router-dom";
import EXIF from "exif-js";

// material
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import RoomIcon from "@material-ui/icons/Room";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";

// components
import image from "./images";

const useStyles = makeStyles({
	head: {
		backgroundColor: "#2c6975",
		color: "white",
		position: "absolute",
		top: "0%",
		left: "0%",
		width: "100%"
	},
	card: {
		width: "90vw",
		height: "auto",
		marginTop: "7%",
		marginLeft: "5%"
	},
	content: {
		height: "100%"
	},
	upload: {
		height: "355px",
		margin: "5px",
		marginBottom: "10px",
		border: "2px dashed black"
	},
	upload1: {
		height: "355px",
		margin: "5px",
		marginBottom: "10px",
		border: "1px dashed black"
	},
	note: {
		width: "90%",
		padding: "4%"
	}
});

const accessToken = "pk.eyJ1IjoiazIzIiwiYSI6ImNrZHlpeGFsbTExNDgzMHFpMWh0aW4wb2QifQ._Os1vIXrEuuLqt1dQcPf6w";
const URL = "https://api.mapbox.com/geocoding/v5/mapbox.places/";

const Transition = React.forwardRef(function Transition(props, ref) {
	return <Slide direction="up" ref={ref} {...props} />;
});

const validate = (email) => {
	// eslint-disable-next-line
	const expression = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return !expression.test(email);
};

const User = () => {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [imageSrc, setImageSrc] = useState("");
	const [imagePath, setImagePath] = useState("");
	const [location, setLocation] = useState({ name: "", lat: "", long: "" });
	const [userInfo, setUserInfo] = useState({ email: "", phone: "" });
	const [errorEmail, setErrorEmail] = useState(false);
	const [errorPhone, setErrorPhone] = useState(false);
	const [error, setError] = useState(null);
	const [redirectAdmin, setRedirectAdmin] = useState(false);
	const [redirectWorker, setRedirectWorker] = useState(false);

	const onDrop = (files) => {
		let path = files[0].path.replace(".jpg", "");
		setImagePath(image[path]);
		setImageSrc(files[0]);

		EXIF.getData(files[0], () => {
			if (Object.keys(files[0].exifdata).length > 0) {
				const Coordinates = {
					lat: (files[0].exifdata.GPSLatitude[0] + files[0].exifdata.GPSLatitude[1] / 60 + files[0].exifdata.GPSLatitude[2] / 3600).toFixed(6),
					long: (files[0].exifdata.GPSLongitude[0] + files[0].exifdata.GPSLongitude[1] / 60 + files[0].exifdata.GPSLongitude[2] / 3600).toFixed(6)
				};
				console.log(Coordinates);
				axios.get(`${URL}${Coordinates.long},${Coordinates.lat}.json?access_token=${accessToken}`).then((res) => {
					setLocation({ name: res.data.features[0].place_name, lat: Coordinates.lat, long: Coordinates.long });
				});
			} else {
				alert("Add Location of the Image");
			}
		});
	};

	const onSubmit = () => {
		if (errorPhone === false && errorEmail === false) {
			console.log(imageSrc);
			var image = new FormData();
			image.append("file", imageSrc);
			// Image Upload.
			axios
				.post("http://localhost:5965/upload", image, {
					headers: {
						"Content-Type": "multipart/form-data"
					}
				})
				.then((res) => {
					if (res.data.result === "uploaded successfully") {
						// Garbage Checkup
						axios
							.post(`http://127.0.0.1:5965/predict?path=Upload_image/${imageSrc.path}`)
							.then((res) => {
								console.log(res.data);
								if (res.data.result === "Garbage") {
									const data = new URLSearchParams();
									data.append("email", userInfo.email);
									data.append("phone", userInfo.phone);
									data.append("location", location.name);
									data.append("cleaned", 0);
									data.append("assigned", 0);
									data.append("enviornment_type", res.data.Enviornment_type);
									if (location.lat === "" && location.long === "") {
										axios.get(`${URL}${location}.json?access_token=${accessToken}`).then((res) => {
											data.append("lat", res.data.features[0].center[1]);
											data.append("long", res.data.features[0].center[0]);
										});
									} else {
										data.append("lat", location.lat);
										data.append("long", location.long);
									}
									// Complain Addition

									axios
										.post("http://localhost:8000/api/complain/create", data, {
											headers: {
												"content-type": "application/x-www-form-urlencoded"
											}
										})
										.then((res) => {
											alert("Thankyou Your Complaint has been noted");
											setUserInfo({ email: "", phone: "" });
											setLocation({ name: "", lat: "", long: "" });
										})
										.catch((err) => {
											alert("Error Occured: " + err);
										});
								}
							})
							.catch((err) => {
								alert("Error: " + err);
							});
					}
				})
				.catch((err) => {
					alert("Error: " + err);
				});

			setError(null);
			setOpen(false);
		} else {
			setError("please fill all data properly");
		}
	};

	const currentLocation = () => {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function (position) {
				let loc = {
					latitude: position.coords.latitude,
					longitude: position.coords.longitude
				};
				axios.get(`${URL}${loc.longitude},${loc.latitude}.json?access_token=${accessToken}`).then((res) => {
					setLocation({ name: res.data.features[0].place_name, lat: loc.latitude, long: loc.longitude });
				});
			});
		}
	};

	return (
		<div>
			<Box display="flex" p={2} className={classes.head}>
				<img src="../../green-earth.png" alt="icon" width="40" height="40" style={{ marginLeft: "50px" }} />
				<Typography variant="h5" component="span" style={{ marginLeft: "5px" }}>
					Cleanliness Monitoring System
				</Typography>
				<Button
					variant="contained"
					color="primary"
					size="small"
					style={{ float: "right", marginLeft: "auto", marginRight: "10px" }}
					onClick={(e) => {
						setRedirectAdmin(true);
					}}>
					Admin Login
				</Button>
				<Button
					variant="contained"
					color="primary"
					size="small"
					style={{ float: "right", marginRight: "90px" }}
					onClick={(e) => {
						setRedirectWorker(true);
					}}>
					Worker Login
				</Button>
			</Box>

			<Card className={classes.card} elevation={5}>
				<CardContent className={classes.content}>
					{imagePath === "" ? (
						<Dropzone onDrop={(acceptedFiles) => onDrop(acceptedFiles)} minSize={1024} maxSize={5072000}>
							{({ getRootProps, getInputProps }) => (
								<div {...getRootProps()}>
									<input {...getInputProps()} />
									<CardActionArea className={classes.upload}>
										<Typography variant="h6" color="textSecondary" style={{ marginLeft: "100px" }}>
											Click Here To Upload Image
										</Typography>
									</CardActionArea>
								</div>
							)}
						</Dropzone>
					) : (
						<Dropzone onDrop={(acceptedFiles) => onDrop(acceptedFiles)} minSize={1024} maxSize={3072000}>
							{({ getRootProps, getInputProps }) => (
								<div {...getRootProps()}>
									<input {...getInputProps()} />
									<CardActionArea className={classes.upload1}>
										<CardMedia component="img" alt="Upload Image First" height="355" image={imagePath} />
									</CardActionArea>
								</div>
							)}
						</Dropzone>
					)}
					<Grid container spacing={0} alignItems="flex-end">
						<Grid item xs={12}>
							<TextField
								size="small"
								style={{ marginTop: "5px" }}
								label="Input Location Of Image"
								variant="outlined"
								value={location.name}
								fullWidth
								onChange={(e) => {
									setLocation({ ...location, name: e.target.value });
								}}
							/>
						</Grid>
					</Grid>

					<Typography variant="subtitle2" color="textPrimary" component="h6" style={{ marginTop: "10px", marginLeft: "10px" }}>
						Upload Image of your locality containing waste and garbage along with location
						<Button
							variant="contained"
							size="medium"
							color="primary"
							style={{ float: "right", marginLeft: "10px" }}
							onClick={() => {
								if (location !== "" && imageSrc !== "") {
									setOpen(true);
								} else {
									alert("Please Enter Location && Upload Image");
								}
							}}>
							Submit Complaint
						</Button>
						<Button variant="contained" size="medium" style={{ float: "right", backgroundColor: "lightgreen" }} onClick={currentLocation}>
							<RoomIcon color="inherit" />
							Current Location
						</Button>
					</Typography>
				</CardContent>
			</Card>
			<Dialog
				open={open}
				TransitionComponent={Transition}
				keepMounted
				onClose={() => {
					setOpen(false);
				}}
				aria-labelledby="alert-dialog-slide-title"
				aria-describedby="alert-dialog-slide-description">
				<DialogTitle id="alert-dialog-slide-title">Please Enter Data for Verification</DialogTitle>
				<DialogContent>
					<DialogContentText id="alert-dialog-slide-description">
						{error
							? error
							: "Your data is safe with us, it will only be used to communicate with you for further details about the situation only if needed."}
					</DialogContentText>
					<Grid container spacing={2} alignItems="flex-end">
						<Grid item xs={12}>
							<TextField
								size="medium"
								label="Enter Valid Email"
								variant="outlined"
								value={userInfo.email}
								fullWidth
								onChange={(e) => {
									setUserInfo({ ...userInfo, email: e.target.value });
									setErrorEmail(validate(e.target.value));
								}}
								error={errorEmail}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								type="number"
								size="medium"
								label="Enter Valid Phone"
								variant="outlined"
								value={userInfo.phone}
								fullWidth
								onChange={(e) => {
									setUserInfo({ ...userInfo, phone: e.target.value });
									setErrorPhone(e.target.value.length === 10 ? false : true);
								}}
								error={errorPhone}
							/>
						</Grid>
					</Grid>
				</DialogContent>
				<DialogActions>
					<Button
						onClick={() => {
							setOpen(false);
						}}
						color="secondary"
						variant="contained">
						Cancel
					</Button>
					<Button onClick={onSubmit} color="primary" disabled={errorPhone || errorEmail} variant="contained">
						Submit
					</Button>
				</DialogActions>
			</Dialog>
			{redirectAdmin ? <Redirect to="/login" /> : null}
			{redirectWorker ? <Redirect to="/login_2" /> : null}
		</div>
	);
};

export default User;
