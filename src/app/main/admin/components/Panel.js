// imports
import React, { useState } from "react";
import axios from "axios";

// material UI
import { DataGrid } from "@material-ui/data-grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const useStyles = makeStyles(() => ({
	root: {
		margin: "5px",
		width: "100%"
	},
	root1: {
		margin: "5px",
		height: "400px"
	}
}));

const columns = [
	{ field: "location", headerName: "Location", width: 250 },
	{ field: "total_complain", headerName: "Total Complains", type: "number", width: 80 },
	{ field: "lat", headerName: "Latitude", type: "number", width: 80 },
	{ field: "long", headerName: "Longitude", type: "number", width: 80 },
	{ field: "enviornment_type", headerName: "Enviornment", type: "number", width: 130 },
	{ field: "created_on", headerName: "1st Complain", type: "number", width: 130 },
	{ field: "updated_on", headerName: "latest Complain", type: "number", width: 130 }
];

const columnsWorker = [
	{ field: "id", headerName: "ID", width: 80 },
	{ field: "name", headerName: "Name", width: 230 },
	{ field: "email", headerName: "Email", type: "number", width: 230 },
	{ field: "phone", headerName: "Phone", type: "number", width: 230 }
];

export default function Panel({ data, refresh }) {
	const classes = useStyles();
	const [complain, setComplain] = useState({});
	const [availWorkers, setAvailWorkers] = useState([]);
	const [selectedWorkers, setSelectedWorkers] = useState([]);
	const [open, setOpen] = React.useState(false);

	const assign = (e) => {
		if (selectedWorkers.length > 0) {
			const data = new URLSearchParams();
			data.append("workers", [...selectedWorkers]);
			axios
				.post(
					`http://localhost:8000/api/workers/assign/${encodeURI(complain.location)}/${encodeURI(complain.lat)}/${encodeURI(complain.long)}`,
					data,
					{
						headers: {
							"content-type": "application/x-www-form-urlencoded"
						}
					}
				)
				.then((res) => {
					alert("Worker Assigned Successfully");
					setComplain({});
					setSelectedWorkers([]);
					refresh();
				})
				.catch((err) => {
					alert("Error: " + err);
				});
			setOpen(false);
		} else {
			alert("Select atleast 1 Worker");
		}
	};

	const openDialog = (row) => {
		setComplain({ ...row });
		axios
			.get("http://localhost:8000/api/workers/get_avail")
			.then((res) => {
				setAvailWorkers([...res.data]);
			})
			.catch((err) => {
				alert("Error: " + err);
			});
		setOpen(true);
	};

	return (
		<Paper elevation={5} className={classes.root}>
			<DataGrid rows={data} columns={columns} autoPageSize onRowClick={(params) => openDialog(params.row)} />
			<Dialog fullWidth maxWidth="md" open={open} onClose={() => setOpen(false)}>
				<DialogTitle id="max-width-dialog-title">Assign Worker To Selected Location</DialogTitle>
				<DialogContent>
					<DialogContentText>Select Workers to Location: "{complain.location}" From List of Available Workers</DialogContentText>
					<Paper elevation={5} className={classes.root1}>
						<DataGrid
							rows={availWorkers}
							columns={columnsWorker}
							autoPageSize
							onSelectionChange={(params) => setSelectedWorkers([...params.rowIds])}
							checkboxSelection
						/>
					</Paper>
				</DialogContent>
				<DialogActions>
					<Button onClick={() => setOpen(false)} color="primary" variant="contained">
						Close
					</Button>
					<Button onClick={(e) => assign(e)} color="secondary" variant="contained">
						Assign
					</Button>
				</DialogActions>
			</Dialog>
		</Paper>
	);
}
