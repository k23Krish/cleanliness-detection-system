// imports
import React, { useState } from "react";
import validator from "validator";
import axios from "axios";

// material UI
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(() => ({
	root: {
		margin: "5px",
		width: "100%",
		padding: "10px"
	}
}));

export default function Panel2() {
	const classes = useStyles();
	const [userInfo, setUserInfo] = useState({ name: "", email: "", phone: "" });
	const [errorEmail, setErrorEmail] = useState(false);
	const [errorPhone, setErrorPhone] = useState(false);
	const [errorPass, setErrorPass] = useState(false);
	const [errorName, setErrorName] = useState(false);

	const createWorker = (e) => {
		e.preventDefault();
		if (errorPhone === false && errorEmail === false && errorPass === false && errorName === false) {
			const data = new URLSearchParams();

			data.append("email", userInfo.email);
			data.append("phone", userInfo.phone);
			data.append("name", userInfo.name);
			data.append("password", userInfo.pass);

			axios
				.post("http://localhost:8000/api/workers/create", data, {
					headers: {
						"content-type": "application/x-www-form-urlencoded"
					}
				})
				.then((res) => {
					alert("Worker Added Successfully");
					setUserInfo({ name: "", email: "", phone: "", pass: "" });
				})
				.catch((err) => {
					alert("Error Occured: " + err);
				});
		} else {
			alert("please fill all data properly");
		}
	};

	return (
		<Paper elevation={5} className={classes.root}>
			<form onSubmit={createWorker} noValidate autoComplete="off">
				<TextField
					size="medium"
					label="Enter Name"
					variant="outlined"
					value={userInfo.name}
					fullWidth
					style={{ marginTop: "10px" }}
					onChange={(e) => {
						setUserInfo({ ...userInfo, name: e.target.value });
						setErrorName(!validator.isAlpha(e.target.value));
					}}
					error={errorName}
				/>
				<TextField
					size="medium"
					label="Enter Valid Email"
					variant="outlined"
					value={userInfo.email}
					fullWidth
					style={{ marginTop: "10px" }}
					onChange={(e) => {
						setUserInfo({ ...userInfo, email: e.target.value });
						setErrorEmail(!validator.isEmail(e.target.value));
					}}
					error={errorEmail}
					helperText={errorEmail ? "Enter correct Mail format" : ""}
				/>
				<TextField
					type="number"
					size="medium"
					label="Enter Valid Phone"
					variant="outlined"
					value={userInfo.phone}
					fullWidth
					style={{ marginTop: "10px" }}
					onChange={(e) => {
						setUserInfo({ ...userInfo, phone: e.target.value });
						setErrorPhone(e.target.value.length === 10 ? false : true);
					}}
					error={errorPhone}
					helperText={errorPhone ? "Enter 10 digit phone numnber" : ""}
				/>
				<TextField
					value={userInfo.pass}
					className={classes.form}
					onChange={(e) => {
						setErrorPass(!validator.isLength(e.target.value, { min: 4 }));
						setUserInfo({ ...userInfo, pass: e.target.value });
					}}
					error={errorPass}
					id="outlined-basic"
					label="Password"
					variant="outlined"
					type="password"
					fullWidth
					style={{ marginTop: "10px" }}
					helperText={errorPass ? "Length Should Be Greater than 4" : ""}
				/>
				<Button type="submit" color="primary" disabled={errorPhone || errorEmail} style={{ marginTop: "10px" }} variant="contained">
					Submit
				</Button>
			</form>
		</Paper>
	);
}
