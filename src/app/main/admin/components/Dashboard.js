// imports
import React, { Fragment, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";

// material
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";

// mapbox
import MapGL from "react-map-gl";
import Pins from "./Pins";

// components
import Panel from "./Panel";
import Panel1 from "./Panel1";
import Panel2 from "./Panel2";
import Panel3 from "./Panel3";

const MAPBOX_TOKEN = "pk.eyJ1IjoiazIzIiwiYSI6ImNrZHlpeGFsbTExNDgzMHFpMWh0aW4wb2QifQ._Os1vIXrEuuLqt1dQcPf6w";

const useStyles = makeStyles(() => ({
	head: {
		backgroundColor: "#2c6975",
		color: "white",
		width: "100%"
	},
	root: {
		display: "inline-flex",
		width: "90vw",
		height: "80vh"
	}
}));

const Dashboard = ({ cookie, setCookie }) => {
	const classes = useStyles();
	const [viewport, setViewPort] = useState({
		latitude: 19.271959118373385,
		longitude: 76.57495762344449,
		zoom: 5.5,
		bearing: 0,
		pitch: 0
	});
	const [page, setPage] = useState(0);
	const [complain, setComplain] = useState([]);

	const _onViewportChange = (viewport) => {
		setViewPort({ ...viewport });
	};

	const getData = () => {
		axios
			.get("http://localhost:8000/api/complain/unClean")
			.then((res) => {
				setComplain([...res.data]);
			})
			.catch((err) => {
				alert("Error: " + err);
			});
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<Fragment>
			<Grid container spacing={5} direction="column" justify="center" alignItems="center">
				<Grid className={classes.head} item>
					<img src="../../green-earth.png" alt="icon" width="40" height="40" style={{ marginLeft: "50px" }} />
					<Typography variant="h5" component="span" style={{ marginLeft: "5px" }}>
						Cleanliness Monitoring System
					</Typography>
					<Button
						variant="contained"
						color="secondary"
						size="small"
						startIcon={<ExitToAppRoundedIcon />}
						style={{ float: "right", marginTop: "15px", marginRight: "60px" }}
						onClick={(e) => {
							setCookie("loginAdmin", false);
						}}>
						Logout
					</Button>
					<ButtonGroup variant="contained" color="primary" size="small" style={{ float: "right", marginTop: "15px", marginRight: "60px" }}>
						<Button color={page === 0 ? "default" : "primary"} onClick={(e) => setPage(0)}>
							Latest Complains
						</Button>
						<Button color={page === 1 ? "default" : "primary"} onClick={(e) => setPage(1)}>
							Assigned Areas
						</Button>
						<Button color={page === 2 ? "default" : "primary"} onClick={(e) => setPage(2)}>
							History
						</Button>
						<Button color={page === 3 ? "default" : "primary"} onClick={(e) => setPage(3)}>
							Add Worker
						</Button>
					</ButtonGroup>
				</Grid>

				<Grid item xs={12}>
					<Paper className={classes.root} elevation={10}>
						{page === 0 ? (
							<MapGL
								{...viewport}
								width="50%"
								height="100%"
								mapStyle="mapbox://styles/mapbox/dark-v9"
								onViewportChange={(e) => _onViewportChange(e)}
								mapboxApiAccessToken={MAPBOX_TOKEN}>
								<Pins data={complain} />
							</MapGL>
						) : null}

						{page === 0 ? (
							<Panel data={complain} refresh={getData} />
						) : page === 1 ? (
							<Panel1 />
						) : page === 2 ? (
							<Panel2 />
						) : page === 3 ? (
							<Panel3 />
						) : null}
					</Paper>
				</Grid>
			</Grid>
			{cookie.loginAdmin === "true" ? <Redirect to="/admin" /> : <Redirect to="/login" />}
		</Fragment>
	);
};

export default Dashboard;
