// imports
import React, { useState, useEffect } from "react";
import axios from "axios";

// material UI
import { DataGrid } from "@material-ui/data-grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(() => ({
	root: {
		margin: "5px",
		width: "100%"
	}
}));

const columns = [
	{ field: "location", headerName: "Location", width: 250 },
	{ field: "cleaned_by", headerName: "Cleaned By", width: 250 },
	{ field: "total_complain", headerName: "Total Complains", type: "number", width: 180 },
	{ field: "lat", headerName: "Latitude", type: "number", width: 80 },
	{ field: "long", headerName: "Longitude", type: "number", width: 80 },
	{ field: "enviornment_type", headerName: "Enviornment", type: "number", width: 150 },
	{ field: "updated_on", headerName: "Complain Date", type: "number", width: 150 },
	{ field: "assigned_on", headerName: "Assigned Date", type: "number", width: 150 }
];

export default function Panel() {
	const classes = useStyles();
	const [data, setData] = useState([]);

	useEffect(() => {
		axios
			.get("http://localhost:8000/api/complain/unClean_A")
			.then((res) => {
				let ids;
				let cleanedBy = "";
				res.data.forEach((complain) => {
					ids = complain.assigned_to.split(",");

					ids.forEach((id) => {
						axios.get(`http://localhost:8000/api/workers/${id}`).then((res) => {
							cleanedBy = cleanedBy + `Id: ${res.data[0].id} Name: ${res.data[0].name}, `;
							setData((prevState, props) => {
								return [...prevState, { ...complain, cleaned_by: cleanedBy }];
							});
						});
					});
				});
			})
			.catch((err) => {
				alert("Error: " + err);
			});
		// eslint-disable-next-line
	}, []);

	return (
		<Paper elevation={5} className={classes.root}>
			<DataGrid rows={data} columns={columns} autoPageSize />
		</Paper>
	);
}
