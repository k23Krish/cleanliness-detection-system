// imports
import React, { useState, useEffect } from "react";
import axios from "axios";

// material UI
import { DataGrid } from "@material-ui/data-grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles(() => ({
	root: {
		margin: "5px",
		width: "100%"
	}
}));

const columns = [
	{ field: "location", headerName: "Location", width: 250 },
	{ field: "cleaned_by", headerName: "Cleaned By", width: 250 },
	{ field: "total_complain", headerName: "Total Complains", type: "number", width: 80 },
	{ field: "lat", headerName: "Latitude", type: "number", width: 80 },
	{ field: "long", headerName: "Longitude", type: "number", width: 80 },
	{ field: "enviornment_type", headerName: "Enviornment", type: "number", width: 150 },
	{ field: "updated_on", headerName: "Complain Date", type: "number", width: 150 },
	{ field: "assigned_on", headerName: "Assigned Date", type: "number", width: 150 },
	{ field: "cleaned_on", headerName: "Cleaned Date", type: "number", width: 150 }
];

export default function Panel2() {
	const classes = useStyles();
	const [data, setData] = useState([]);

	useEffect(() => {
		axios
			.get("http://localhost:8000/api/complain/record")
			.then((res) => {
				extractData(res.data);
			})
			.catch((err) => {
				alert("Error: " + err);
			});

		// eslint-disable-next-line
	}, []);

	const extractData = (complains) => {
		complains.forEach((complain) => {
			let cleanedBy = "";
			let ids = complain.assigned_to.split(",");
			ids.forEach((id) => {
				axios.get(`http://localhost:8000/api/workers/${id}`).then((res) => {
					cleanedBy = cleanedBy + `Name: ${res.data[0].name}, `;
					setData((prevState, props) => {
						return [...prevState, { ...complain, cleaned_by: cleanedBy }];
					});
				});
			});
		});
	};

	return (
		<Paper elevation={5} className={classes.root}>
			<DataGrid rows={data} columns={columns} autoPageSize />
		</Paper>
	);
}
