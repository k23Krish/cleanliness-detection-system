import street0 from "./street0.jpg";
import street1 from "./street1.jpg";
import street2 from "./street2.jpg";
import street10 from "./street10.jpg";
import street11 from "./street11.jpg";
import street12 from "./street12.jpg";

export default {
	street0,
	street1,
	street2,
	street10,
	street11,
	street12
};
