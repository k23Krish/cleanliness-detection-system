// imports
import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone";
import axios from "axios";
import EXIF from "exif-js";

// material

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

// components
import image from "./images";

const useStyles = makeStyles({
	head: {
		backgroundColor: "#2c6975",
		color: "white",
		position: "absolute",
		top: "0%",
		left: "0%",
		width: "100%"
	},
	card: {
		margin: "2px"
	},
	content: {
		height: "100%"
	},
	upload: {
		height: "355px",
		margin: "5px",
		marginBottom: "10px",
		border: "2px dashed black"
	},
	upload1: {
		height: "355px",
		margin: "5px",
		marginBottom: "10px",
		border: "1px dashed black"
	},
	note: {
		width: "90%",
		padding: "4%"
	}
});

const distance = (lat1, lon1, lat2, lon2, refresh) => {
	var R = 6371; // Earth's radius in Km
	return Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1)) * R;
};

// const isDateInThisWeek = (date) => {
// 	const now = new Date().getMonth();
// 	return date.getMonth() === now;
// };

const Upload = ({ close, next, loc, lat, long }) => {
	const classes = useStyles();
	const [imageSrc, setImageSrc] = useState("");
	const [imagePath, setImagePath] = useState("");
	const [location, setLocation] = useState({ name: loc, lat: lat, long: long });
	const [enable, setEnable] = useState(true);

	const onDrop = (files) => {
		console.log(files[0]);
		let path = files[0].path.replace(".jpg", "");
		setImagePath(image[path]);
		setImageSrc(files[0]);

		EXIF.getData(files[0], () => {
			if (Object.keys(files[0].exifdata).length > 0) {
				const Coordinates = {
					lat: (files[0].exifdata.GPSLatitude[0] + files[0].exifdata.GPSLatitude[1] / 60 + files[0].exifdata.GPSLatitude[2] / 3600).toFixed(6),
					long: (files[0].exifdata.GPSLongitude[0] + files[0].exifdata.GPSLongitude[1] / 60 + files[0].exifdata.GPSLongitude[2] / 3600).toFixed(6)
				};
				if (
					distance(Coordinates.lat, Coordinates.long, location.lat, location.long) <= 5 ||
					(Coordinates.lat === location.lat && Coordinates.long === location.long)
				) {
					setEnable(false);
					// if (isDateInThisWeek(new Date(files[0].exifdata.GPSDateTime))) {
					// 	setEnable(false);
					// } else {
					// 	alert("Please Upload Image of Current Month Only");
					// 	setEnable(true);
					// }
				} else {
					setEnable(true);
					alert("Please Upload Image of Same Location as Assigned to You.");
				}
			} else {
				alert("Add A Image with GeoTag Only");
				setImageSrc("");
				setImagePath("");
			}
		});
	};

	const onSubmit = () => {
		var image = new FormData();
		image.append("file", imageSrc);
		// Image Upload.
		axios
			.post("http://localhost:5965/upload", image, {
				headers: {
					"Content-Type": "multipart/form-data"
				}
			})
			.then((res) => {
				if (res.data.result === "uploaded successfully") {
					// Garbage Checkup
					axios
						.post(`http://127.0.0.1:5965/predict?path=Upload_image/${imageSrc.path}`)
						.then((res) => {
							console.log(res.data);
							if (res.data.result === "Garbage") {
								alert("Please Upload A proper Image Or Check if Area Is Cleaned Properly or Not");
							} else {
								// Clear Area
								axios
									.post(`http://localhost:8000/api/complain/area_cleaned/${location.name}`)
									.then((res) => {
										next();
										close();
									})
									.catch((err) => alert("Error: " + err));
							}
						})
						.catch((err) => {
							alert("Error: " + err);
						});
				}
			})
			.catch((err) => {
				alert("Error: " + err);
			});
	};

	return (
		<div>
			<Card className={classes.card} elevation={5}>
				<CardContent className={classes.content}>
					{imagePath === "" ? (
						<Dropzone onDrop={(acceptedFiles) => onDrop(acceptedFiles)} minSize={1024} maxSize={5072000}>
							{({ getRootProps, getInputProps }) => (
								<div {...getRootProps()}>
									<input {...getInputProps()} />
									<CardActionArea className={classes.upload}>
										<Typography variant="h6" color="textSecondary" style={{ marginLeft: "100px" }}>
											Click Here To Upload Image
										</Typography>
									</CardActionArea>
								</div>
							)}
						</Dropzone>
					) : (
						<Dropzone onDrop={(acceptedFiles) => onDrop(acceptedFiles)} minSize={1024} maxSize={3072000}>
							{({ getRootProps, getInputProps }) => (
								<div {...getRootProps()}>
									<input {...getInputProps()} />
									<CardActionArea className={classes.upload1}>
										<CardMedia component="img" alt="Upload Image First" height="355" image={imagePath} />
									</CardActionArea>
								</div>
							)}
						</Dropzone>
					)}
					<Grid container spacing={0} alignItems="flex-end">
						<Grid item xs={12}>
							<TextField
								size="small"
								style={{ marginTop: "5px" }}
								label="Input Location Of Image"
								variant="outlined"
								value={location.name}
								fullWidth
								onChange={(e) => {
									setLocation({ ...location, name: e.target.value });
								}}
							/>
						</Grid>
					</Grid>

					<Typography variant="subtitle2" color="textPrimary" component="h6" style={{ marginTop: "10px", marginLeft: "10px" }}>
						Upload Latest Image & Correct Location
						<Button
							variant="contained"
							size="small"
							color="primary"
							style={{ float: "right" }}
							disabled={enable}
							onClick={() => {
								if (location !== "" && imageSrc !== "") {
									onSubmit();
								} else {
									alert("Please Enter Location && Upload Image");
								}
							}}>
							Submit
						</Button>
					</Typography>
				</CardContent>
			</Card>
		</div>
	);
};

export default Upload;
