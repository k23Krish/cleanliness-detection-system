// imports
import React, { Fragment, useState } from "react";
import validator from "validator";
import { Redirect } from "react-router-dom";
import axios from "axios";

// material
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles(() => ({
	head: {
		backgroundColor: "#2c6975",
		color: "white",
		width: "100%"
	},
	root: {
		marginTop: "10vh"
	},
	form: {
		margin: "20px",
		width: "90%"
	}
}));
const Login2 = ({ cookie, setCookie, cookie2, setCookie2 }) => {
	const classes = useStyles();
	const [email, setEmail] = useState("");
	const [pass, setPass] = useState("");
	const [errorEmail, setErrorEmail] = useState(false);
	const [errorPass, setErrorPass] = useState(false);
	const [redirect, setRedirect] = useState(false);

	const loginSend = (e) => {
		e.preventDefault();
		const data = new URLSearchParams();
		data.append("email", email);
		data.append("password", pass);
		axios
			.post("http://localhost:8000/api/workers/login", data, {
				headers: {
					"content-type": "application/x-www-form-urlencoded"
				}
			})
			.then((res) => {
				setCookie2("worker", res.data);
				setCookie("loginWorker", true);
			})
			.catch((err) => {
				alert("Login Failed");
			});
	};

	return (
		<Fragment>
			<Grid container spacing={5} direction="column" justify="center" alignItems="center">
				<Grid className={classes.head} item>
					<img src="../../green-earth.png" alt="icon" width="40" height="40" style={{ marginLeft: "50px" }} />
					<Typography variant="h5" component="span" style={{ marginLeft: "5px" }}>
						Cleanliness Monitoring System
					</Typography>
					<Button
						variant="contained"
						color="primary"
						size="medium"
						style={{ float: "right", marginTop: "15px", marginRight: "80px" }}
						onClick={(e) => {
							setRedirect(true);
						}}>
						Home Page
					</Button>
				</Grid>
				<Grid item xs={5}>
					<Paper className={classes.root} elevation={10}>
						<form onSubmit={loginSend}>
							<TextField
								className={classes.form}
								value={email}
								onChange={(e) => {
									setErrorEmail(!validator.isEmail(e.target.value));
									setEmail(e.target.value);
								}}
								error={errorEmail}
								id="outlined-basic"
								label="Email"
								variant="outlined"
								helperText={errorEmail ? "Invalid Email" : ""}
							/>
							<TextField
								value={pass}
								className={classes.form}
								onChange={(e) => {
									setErrorPass(!validator.isLength(e.target.value, { min: 4 }));
									setPass(e.target.value);
								}}
								error={errorPass}
								id="outlined-basic"
								label="Password"
								variant="outlined"
								type="password"
								helperText={errorPass ? "Length Should Be Greater than 4" : ""}
							/>
							<Button type="submit" className={classes.form} variant="contained" color="primary">
								Login
							</Button>
						</form>
					</Paper>
				</Grid>
			</Grid>
			{cookie.loginWorker === "true" ? <Redirect to="/worker_page" /> : null}
			{cookie.loginWorker === "false" ? <Redirect to="/login_2" /> : null}
			{redirect ? <Redirect to="/" /> : null}
		</Fragment>
	);
};

export default Login2;
