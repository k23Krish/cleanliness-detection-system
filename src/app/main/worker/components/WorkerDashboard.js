// imports
import React, { Fragment, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";

// material
import { DataGrid } from "@material-ui/data-grid";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import CheckRoundedIcon from "@material-ui/icons/CheckRounded";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

// components
import Upload from "../upload";
const useStyles = makeStyles(() => ({
	head: {
		backgroundColor: "#2c6975",
		color: "white",
		width: "100%"
	},
	root: {
		margin: "10px",
		width: "1200px",
		height: "300px"
	},
	form: {
		margin: "20px",
		width: "90%"
	}
}));

const columns = [
	{ field: "location", headerName: "Location", width: 350 },
	{ field: "total_complain", headerName: "Total Complains", type: "number", width: 100 },
	{ field: "lat", headerName: "Latitude", type: "number", width: 100 },
	{ field: "long", headerName: "Longitude", type: "number", width: 100 },
	{ field: "enviornment_type", headerName: "Enviornment", type: "number", width: 150 },
	{ field: "assigned_on", headerName: "Assigned Date", type: "number", width: 190 },
	{ field: "cleaned_on", headerName: "Cleaned Date", type: "number", width: 190 }
];

const WorkerDashboard = ({ cookie, setCookie, cookie2, setCookie2 }) => {
	const classes = useStyles();
	const [open, setOpen] = useState(false);
	const [work, setWork] = useState([]);
	const [completed, setCompleted] = useState([]);
	const [page, setPage] = useState(0);

	const handleDiaClose = () => {
		setOpen(false);
	};
	const areaCleaned = () => {
		alert(`Location Cleared Successfully!!!`);
		getData();
	};

	const completWork = () => {
		setPage(1);
		axios
			.get(`http://localhost:8000/api/complain/record`)
			.then((res) => {
				setCompleted([...res.data]);
			})
			.catch((err) => {
				alert("Error: " + err);
			});
	};

	const getData = () => {
		axios
			.get(`http://localhost:8000/api/complain/${encodeURI(cookie2.worker.data[0].location_assigned)}`)
			.then((res) => {
				setWork([...res.data]);
			})
			.catch((err) => {
				alert("Error: " + err);
			});
	};

	useEffect(() => {
		if (cookie2.worker.error === false) {
			getData();
		} else {
			setCookie("loginWorker", false);
			setCookie("worker", null);
		}
		// eslint-disable-next-line
	}, [cookie2.worker]);

	return (
		<Fragment>
			{cookie.loginWorker === "false" ? <Redirect to="/login_2" /> : null}
			<Grid container spacing={5} direction="column" justify="center" alignItems="center">
				<Grid className={classes.head} item>
					<img src="../../green-earth.png" alt="icon" width="40" height="40" style={{ marginLeft: "50px" }} />
					<Typography variant="h5" component="span" style={{ marginLeft: "5px" }}>
						Cleanliness Monitoring System
					</Typography>
					<Button
						variant="contained"
						color="secondary"
						size="small"
						startIcon={<ExitToAppRoundedIcon />}
						style={{ float: "right", marginTop: "15px", marginRight: "60px" }}
						onClick={(e) => {
							setCookie2("worker", null);
							setCookie("loginWorker", "false");
						}}>
						Logout
					</Button>
					<ButtonGroup variant="contained" color="primary" size="small" style={{ float: "right", marginTop: "15px", marginRight: "60px" }}>
						<Button color={page === 0 ? "default" : "primary"} onClick={(e) => setPage(0)}>
							Current Task
						</Button>
						<Button color={page === 1 ? "default" : "primary"} onClick={(e) => completWork(e)}>
							Completed Task
						</Button>
					</ButtonGroup>
				</Grid>
				{page === 0 ? (
					<Grid item xs={12}>
						<Paper className={classes.root} elevation={10}>
							<DataGrid rows={work} columns={columns} autoPageSize />
						</Paper>
						<Button variant="contained" style={{ margin: "10px", backgroundColor: "lightgreen", float: "right" }} onClick={() => setOpen(true)}>
							<CheckRoundedIcon style={{ marginRight: "5px" }} /> Area Cleaned
						</Button>
					</Grid>
				) : page === 1 ? (
					<Grid item xs={12}>
						<Paper className={classes.root} elevation={10}>
							<DataGrid rows={completed} columns={columns} autoPageSize />
						</Paper>
					</Grid>
				) : null}
			</Grid>
			<Dialog fullWidth maxWidth="md" open={open} onClose={handleDiaClose} aria-labelledby="max-width-dialog-title">
				<DialogTitle id="max-width-dialog-title">Verify Location Cleaned: </DialogTitle>
				<DialogContent>
					{cookie2.worker !== "null" ? (
						<Upload
							close={handleDiaClose}
							next={areaCleaned}
							loc={cookie2.worker.data[0].location_assigned}
							lat={cookie2.worker.data[0].lat}
							long={cookie2.worker.data[0].longi}
						/>
					) : null}
				</DialogContent>
				<DialogActions>
					<Button onClick={handleDiaClose} color="secondary" variant="contained">
						Close
					</Button>
				</DialogActions>
			</Dialog>
		</Fragment>
	);
};

export default WorkerDashboard;
