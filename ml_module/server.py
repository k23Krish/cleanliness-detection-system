# imports
import os
from flask import Flask, flash, jsonify, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin

# components
from GarbageDetectionTest import DetectGarbage
from GarbageClassificationTest import ClassifyGarbage
# from GarbageTypeClassificationTest import ClassifyTypeGarbage

UPLOAD_FOLDER = 'Upload_image'
ALLOWED_EXTENSIONS = {'jpg'}

app = Flask("__GarbageDetector__")
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.secret_key = os.urandom(24)
CORS(app, support_credentials=True)


object = DetectGarbage()
obejctclass = ClassifyGarbage()
# typeClass = ClassifyTypeGarbage()


@app.route("/predict", methods=["GET", "POST"])
@cross_origin(origin='*')
def predict():
   query_parameters = request.args
   path = query_parameters.get('path')

   result = object.classify(path)
   if result == "Garbage":
      Enviornment_type = obejctclass.classify(path)
      # type = typeClass.classify(path)

   else:
      Enviornment_type = "NULL"
      # type = "NULL"
   ret = {"result": result, "Enviornment_type": Enviornment_type}
   return jsonify(ret)


def allowed_file(filename):
   return '.' in filename and \
          filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/upload', methods=["GET", "POST"])
@cross_origin(origin='*')
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return "0k"
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return jsonify({"result": "uploaded successfully"})
    return 0

app.run(debug=True, host='localhost', port=5965)
